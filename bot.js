const mineflayer = require('mineflayer')
const autoeat = require('mineflayer-auto-eat').plugin
const { mineflayer: mineflayerViewer } = require('prismarine-viewer')
const pathfinder = require('mineflayer-pathfinder').pathfinder
const Movements = require('mineflayer-pathfinder').Movements
const { GoalNear } = require('mineflayer-pathfinder').goals

const bot = mineflayer.createBot({
  host: '192.168.1.18', // minecraft server ip
  username: 'Darlling', // minecraft username
  auth: 'offline', // for offline mode servers, you can set this to 'offline'
  port: 49358,                // only set if you need a port that isn't 25565
  version: '1.19.2',             // only set if you need a specific version or snapshot (ie: "1.8.9" or "1.16.5"), otherwise it's set automatically
  // ie: '1.19.5',
  // password: 'negoneynomeiofio!'        // set if you want to use password-based auth (may be unreliable)
})

if (bot) {

  bot.loadPlugin(autoeat)

  bot.loadPlugin(require('mineflayer-dashboard'))

  bot.loadPlugin(pathfinder)

  
  bot.once('spawn', () => {
    // mineflayerViewer(bot, { port: 63605, firstPerson: true }) // port is the minecraft server port, if first person is false, you get a bird's-eye view
    bot.autoEat.options.priority = 'saturation'
    bot.autoEat.options.startAt = 16
  })
  
  // bot.on('chat', (username, message) => {
  //   if (username === bot.username) return
  //   bot.chat(message)
  // })

console.log('ta tendo bot');
  bot.once('spawn', () => {
    const defaultMove = new Movements(bot)
    
    bot.on('chat', function(username, message) {

      if (username === bot.username) return
  
      const target = bot.players[username] ? bot.players[username].entity : null


      if (message === 'me siga ' + bot.username) {

        bot.chat('Ok, estou indo')

        bot.pathfinder.setMovements(defaultMove);
        bot.pathfinder.setGoal(!new GoalNear(p.x, p.y, p.z))


        if (!target) {
          bot.chat('Não te vejo!')
        } else {
          bot.chat('Estou te seguindo')
        }

        if (message === 'pare' + bot.username) {

          bot.pathfinder.setGoal(null)
        }

      }

      if (message === 'vem ' + bot.username) {

        if (!target) {
          
          bot.chat('I don\'t see you !')
          return;
        }
        const p = target.position

        if (username != 'Gugueta') {

          bot.chat('Eu nao te pertenço' + username)
          return;
          
        } else {
          bot.chat('Fala chefe, to indo')

        }
        bot.pathfinder.setMovements(defaultMove)
        bot.pathfinder.setGoal(new GoalNear(p.x, p.y, p.z))
      } 
    })
  })

  bot.once('inject_allowed', () => {
    global.console.log = bot.dashboard.log
    global.console.error = bot.dashboard.log
  })


  bot.once('spawn', () => {

    bot.autoEat.enable()

  })

  bot.on('autoeat_started', (item, offhand) => {
      console.log(`Eating ${item.name} in ${offhand ? 'offhand' : 'hand'}`)
  })

  bot.on('autoeat_error', (error) => {
      console.error(error)
  })

  bot.on('autoeat_finished', (item, offhand) => {
      console.log(`Finished eating ${item.name} in ${offhand ? 'offhand' : 'hand'}`)
  })

  bot.on('kicked', console.log)
  bot.on('error', console.log)
} else {
  console.log('error');
}
